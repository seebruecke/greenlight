# frozen_string_literal: true

class AddLocalgroupToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :localgroup, :string
  end
end
